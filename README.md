# License Manager - docker-compose

## If you want to run the app, you must follow the next steps:

### 1. In the same folder you must have "license-manager-angular", "license-manager-api" and "docker-compose" folders. You can pull this from GIT.

### 2. Go into "license-manager-api" and run the following command: 
`gradle build docker`

### 3. Come back to "docker-compose" folder and run the following command:
`docker-compose up`

### Credentials for admin user:
- username: admin@license-manager.com
- password: admin1234
